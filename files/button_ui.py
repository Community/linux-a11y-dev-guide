import os
import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk

@Gtk.Template(filename=os.path.join(os.path.dirname(__file__), 'button_good.ui'))
class Window(Gtk.ApplicationWindow):
    __gtype_name__ = 'window'
    example_button = Gtk.Template.Child()

    @Gtk.Template.Callback
    def example_button_clicked(self, btn):
        print("hi")
        dlg = Gtk.AlertDialog(message="You clicked the button.")
        dlg.show()

def on_activate(app):
    win = Window(application=app)
    win.present()
    
app = Gtk.Application(application_id='com.example.labeled_button')
app.connect('activate', on_activate)
app.run()