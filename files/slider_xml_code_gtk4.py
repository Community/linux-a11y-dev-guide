import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk


ui_string = """                                                                                                                                                                               
<interface>
  <object class="GtkAdjustment" id="adjustment2">
    <property name="upper">4</property>
    <property name="value">2</property>
    <property name="step-increment">0.1</property>
    <property name="page-increment">1</property>
  </object>
  <template class="window" parent="GtkApplicationWindow">
    <property name="title" translatable="yes">GTK4 Scale/Slider XML Demo</property>
    <property name="resizable">0</property>
    <child>
      <object class="GtkGrid" id="grid1">
        <property name="row-spacing">10</property>
        <property name="column-spacing">10</property>
        <property name="margin-start">20</property>
        <property name="margin-end">20</property>
        <property name="margin-top">20</property>
        <property name="margin-bottom">20</property>
        <child>
          <object class="GtkScale" id="scale_marks">
            <property name="width-request">200</property>
            <property name="draw-value">0</property>
            <property name="adjustment">adjustment2</property>
            <property name="hexpand">1</property>
            <marks>
              <mark value="0" position="bottom">0</mark>
              <mark value="1" position="bottom">1</mark>
              <mark value="2" position="bottom">2</mark>
              <mark value="3" position="bottom">3</mark>
              <mark value="4" position="bottom">4</mark>
            </marks>
          </object>
        </child>
      </object>
    </child>
  </template>
</interface>                                                                                                                                                                       
"""

@Gtk.Template(string=ui_string)
class Window(Gtk.ApplicationWindow):
    __gtype_name__ = 'window'
    scale_marks = Gtk.Template.Child()

    @Gtk.Template.Callback
    def slider_changed(self, slider):
        print(int(slider.get_value()))

def on_activate(app):
    win = Window(application=app)
    win.present()
    
app = Gtk.Application(application_id='com.example.slider')
app.connect('activate', on_activate)
app.run()
