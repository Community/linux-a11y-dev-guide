import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class SimpleMenuWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Gtk3 Simple Menu Demo")
        self.set_default_size(300, 50)
        
        # Create the menu bar
        self.menubar=Gtk.MenuBar()  
        self.file_menu=Gtk.Menu()
        self.edit_menu=Gtk.Menu()

        # Create the File submenu with the items "Open," "Save," and "Exit." Attach it to the menubar.
        self.file_submenu1=Gtk.MenuItem("File") 
        self.file_submenu1.set_submenu(self.file_menu)
        self.menubar.append(self.file_submenu1)
        
        self.file_submenu2=Gtk.MenuItem("Open")
        self.file_submenu2.connect("activate", self.open_file)
        self.file_menu.append(self.file_submenu2)

        self.file_submenu3=Gtk.MenuItem("Save")
        self.file_submenu3.connect("activate", self.save_file)   
        self.file_menu.append(self.file_submenu3) 
        
        self.file_submenu4=Gtk.MenuItem("Exit")
        self.file_submenu4.connect("activate", self.on_close)   
        self.file_menu.append(self.file_submenu4) 

        # Create the Edit submenu with the items "Cut," "Copy," and "Paste." Attach it to the menubar.
        self.edit_submenu1=Gtk.MenuItem("Edit") 
        self.edit_submenu1.set_submenu(self.edit_menu)
        self.menubar.append(self.edit_submenu1)

        self.edit_submenu2=Gtk.MenuItem("Cut")
        self.edit_submenu2.connect("activate", self.cut_text)
        self.edit_menu.append(self.edit_submenu2)

        self.edit_submenu3=Gtk.MenuItem("Copy")
        self.edit_submenu3.connect("activate", self.copy_text)   
        self.edit_menu.append(self.edit_submenu3) 

        self.edit_submenu4=Gtk.MenuItem("Paste")
        self.edit_submenu4.connect("activate", self.paste_text)   
        self.edit_menu.append(self.edit_submenu4)
       
        # Attach the menubar       
        self.grid=Gtk.Grid()
        self.grid.attach(self.menubar, 0, 0, 1, 1)
        self.add(self.grid)
        
    
    # Methods associated with menu items
    def open_file(self, file_submenu2):
        print("Open File")

    def on_close(self, file_submenu4):
        self.close()

    def save_file(self, file_submenu3):
        print("Save File")

    def cut_text(self, edit_submenu2):
        print("Cut Text")

    def copy_text(self, edit_submenu3):
        print("Copy Text")
    
    def paste_text(self, edit_submenu4):
        print("Paste Text")


win=SimpleMenuWindow()
win.connect("destroy", Gtk.main_quit) 
win.show_all()
Gtk.main()
