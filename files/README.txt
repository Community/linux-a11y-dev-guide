The /files directory is used for any data files (except images),
that should be stored in the repo.
For example:

- PDF documents
- spreadsheets
- videos

Please do not use this directory for any other purposes.
