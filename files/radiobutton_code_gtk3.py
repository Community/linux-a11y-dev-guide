import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class RadioButtonWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title="Gtk3 Radio Button Demo")
        self.set_border_width(10)

        rb_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        rb_box.set_margin_top(margin=20)
        rb_box.set_margin_end(margin=20)
        rb_box.set_margin_bottom(margin=20)
        rb_box.set_margin_start(margin=20)
        self.add(rb_box)

        radio_button1 = Gtk.RadioButton.new_with_label_from_widget(None, "North")
        radio_button1.connect("toggled", self.on_button_toggled, "North")
        rb_box.pack_start(radio_button1, False, False, 0)

        radio_button2 = Gtk.RadioButton.new_with_mnemonic_from_widget(radio_button1, "South")
        radio_button2.connect("toggled", self.on_button_toggled, "South")
        rb_box.pack_start(radio_button2, False, False, 0)

        radio_button3 = Gtk.RadioButton.new_with_mnemonic_from_widget(radio_button1, "East")
        radio_button3.connect("toggled", self.on_button_toggled, "East")
        rb_box.pack_start(radio_button3, False, False, 0)

        radio_button4 = Gtk.RadioButton.new_with_mnemonic_from_widget(radio_button1, "West")
        radio_button4.connect("toggled", self.on_button_toggled, "West")
        rb_box.pack_start(radio_button4, False, False, 0)

    def on_button_toggled(self, my_radio_button, my_radio_button_name):
        if my_radio_button.get_active():
            state = "on"
        else:
            state = "off"
        print(my_radio_button_name, "button state:", state)


win = RadioButtonWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
