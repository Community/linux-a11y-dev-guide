import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk

ui_string = """                                                                                                                                                                               
<interface>
  <object class="GtkApplicationWindow" id="window">
    <property name="title">Switch XML Demo</property>
    <property name="title" translatable="true">GTK4 Switch Demo</property>
    <child>
      <object class="GtkBox">
        <property name="orientation">1</property>
        <child>
          <object class="GtkSwitch" id="switch_on">
            <property name="active">true</property>
            <property name="halign">3</property>
            <property name="margin-bottom">6</property>
          </object>
        </child>
        <child>
          <object class="GtkLabel" id="my_switch_label">
            <property name="label" translatable="true">_Light Switch</property>
            <property name="margin-bottom">30</property>
            <property name="use_underline">True</property>
            <property name="mnemonic_widget">switch_on</property>
          </object>
        </child>
      </object>
    </child>
  </object>
</interface>                                                                                                                                                                               
"""

class MySwitchApp(Gtk.Application):
    def __init__(self, ui_string):
        super().__init__(application_id="org.example.GtkSwitchXML")
        self.ui_string = ui_string
        self.connect('activate', self.on_activate)

    def on_activate(self, app):
        self.builder = Gtk.Builder.new_from_string(self.ui_string, -1)
        window = self.builder.get_object("window")
        window.set_application (self)
        window.present()
        window.set_default_size(350, 50)
            
    def do_startup(self):
       Gtk.Application.do_startup(self)


my_switch_app = MySwitchApp(ui_string)
my_switch_app.run()

