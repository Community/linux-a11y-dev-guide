:author:  Bohdan Milar
:revdate: 2023-05-17
:toc:
include::../include/header-assemblies.adoc[]
= Understanding how low vision users control the computer =

include::../modules/understand.low_vision.vision_disorders.adoc[leveloffset=+1]

include::../modules/understand.low_vision.HW_assistive_technologies.adoc[leveloffset=+1]

include::../modules/understand.low_vision.SW_assistive_technologies.adoc[leveloffset=+1]

include::../modules/understand.low_vision.common_issues_and_requirements.adoc[leveloffset=+1]

include::../modules/understand.low_vision.legal_requirements.adoc[leveloffset=+1]
