:author:  Bohdan Milar
:revdate: 2023-04-12
:toc:
include::../include/header-assemblies.adoc[]
= Standards, legal documents and other info resources =

include::../modules/standards.user_and_admin_doc.adoc[leveloffset=+0]

include::../modules/standards.devel_doc.adoc[leveloffset=+0]

include::../modules/standards.legal_and_standard_doc.adoc[leveloffset=+0]

include::../modules/standards.doc_for_web.adoc[leveloffset=+0]
