== Legal and standard documents ==

- https://wiki.gnome.org/Accessibility/Laws[Relevant Accessibility Laws] at gnome.org
- https://portal.etsi.org/webapp/workprogram/Report_WorkItem.asp?WKI_ID=50127[EU standard: EN 301 549] (https://www.etsi.org/deliver/etsi_en/301500_301599/301549/02.01.02_60/en_301549v020102p.pdf[PDF])
- https://en.wikipedia.org/wiki/Section_508_Amendment_to_the_Rehabilitation_Act_of_1973[Section 508 Amendment to the Rehabilitation Act of 1973]:
  ** https://www.govinfo.gov/content/pkg/USCODE-2017-title29/html/USCODE-2017-title29-chap16-subchapV-sec794d.htm[Rehabilitation Act (29 U.S.C. § 794d)]
  ** https://www.access-board.gov/ict/[U.S. Access Board -- Information and Communication Technology -- Revised 508 Standards and 255 Guidelines]
  ** https://www.epa.gov/accessibility/[US EPA (Environmental Protection Agency): Section 508: Accessibility]
- https://www.aoda.ca/[Accessibility for Ontarians with Disabilities Act]
- https://law.resource.org/pub/us/cfr/ibr/006/hfes.200.2.html[ANSI/HFES 200 -- Human Factors Engineering of Software User Interfaces -- Part 2: Accessibility] (or buy at https://webstore.ansi.org/standards/hfes/ansihfes2002008[ANSI])
- https://www.iso.org/standard/39080.html[ISO 9241-171:2008 -- Ergonomics of human-system interaction -- Part 171: Guidance on software accessibility] -- text not freely available
- https://www.itic.org/policy/accessibility/vpat/[VPAT] -- Templates for Accessibility Conformance Report (ACR)
